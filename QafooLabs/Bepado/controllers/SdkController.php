<?php

class QafooLabs_Bepado_SdkController extends Mage_Core_Controller_Front_Action
{
    public function handleAction()
    {
        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        Mage::register('isSecureArea', true);
        try {
            echo $sdk->handle(file_get_contents('php://input'), $_SERVER);
        } catch (\Exception $e) {
            var_dump((string)$e);
        }
        Mage::unregister('isSecureArea');
    }
}
