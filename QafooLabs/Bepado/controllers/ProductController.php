<?php

class QafooLabs_Bepado_ProductController extends Mage_Adminhtml_Controller_Action
{
    public function exportAction()
    {
        $data = $this->getRequest()->getPost();

        if (!isset($data['product'])) {
            return $this->_redirect('adminhtml/catalog_product/index');
        }

        $productIds = $data['product'];

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_write');

        $exportedProductIds = $this->getAlreadyExportedProductIds($db, $productIds);
        $importedProductIds = $this->getImportedProductIds($db, $productIds);

        $stmt = $db->prepare('INSERT INTO bepado_magento_product_export (bmpe_product_id) VALUES (?)');

        foreach ($productIds as $productId) {
            if (in_array($productId, $importedProductIds)) {
                continue;
            } else if (in_array($productId, $exportedProductIds)) {
                $sdk->recordUpdate($productId);
            } else {
                $sdk->recordInsert($productId);

                $stmt->bindValue(1, $productId);
                $stmt->execute();
            }
        }

        return $this->_redirect('adminhtml/catalog_product/index');
    }

    private function getImportedProductIds($db, $productIds)
    {
        $productIds = array_map(array($db, 'quote'), $productIds);
        $sql = 'SELECT bmpi_product_id FROM bepado_magento_product_import WHERE bmpi_product_id IN (' . implode(', ', $productIds) . ')';

        $stmt = $db->prepare($sql);
        $stmt->execute();

        return array_map(function ($row) {
            return $row['bmpi_product_id'];
        }, $stmt->fetchAll());
    }

    private function getAlreadyExportedProductIds($db, $productIds)
    {
        $productIds = array_map(array($db, 'quote'), $productIds);
        $sql = 'SELECT bmpe_product_id FROM bepado_magento_product_export WHERE bmpe_product_id IN (' . implode(', ', $productIds) . ')';

        $stmt = $db->prepare($sql);
        $stmt->execute();

        return array_map(function ($row) {
            return $row['bmpe_product_id'];
        }, $stmt->fetchAll());
    }
}
