# QafooLabs bepado Module for Magento

Connect your Magento store to bepado

This is an experimental prototype at the moment!  No tests, just hacked

## Todos

* Google Category Ajax Suggest + Label
* Configuration for Attributes
* Configuration for Website/Store
* TaxClass Mapping
* Sanity Checks for Currency (only EUR)
* bepado Purchase Price Attribute on Price
* Handle Updates of Cart with regard to checkProducts()
* Make module resistant against partner shop failures
