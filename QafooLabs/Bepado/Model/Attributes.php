<?php

class QafooLabs_Bepado_Model_Attributes
{
    private $data = array();

    public function __construct(array $data = array())
    {
        $this->data = $data;
    }

    public function addConfigAttribute($name, $magentoConfig)
    {
        $this->data[$name] = Mage::getStoreConfig($magentoConfig);
        return $this;
    }

    public function __get($name)
    {
        if ($name === null || !isset($this->data[$name])) {
            return null;
        }

        return $this->data[$name];
    }
}
