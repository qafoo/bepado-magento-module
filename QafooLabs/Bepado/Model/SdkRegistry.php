<?php

class QafooLabs_Bepado_Model_SdkRegistry
{
    private $sdk;

    public function getSDK()
    {
        if ($this->sdk === null) {
            $this->sdk = $this->createSDK();
        }

        return $this->sdk;
    }

    private function createSDK()
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $attributes = new QafooLabs_Bepado_Model_Attributes();
        $attributes->addConfigAttribute('deliveryWorkDays', 'bepado_attributes/product/deliveryworkdays');

        $version = Mage::getConfig()->getModuleConfig("QafooLabs_Bepado")->version;
        $builder = new \Bepado\SDK\SDKBuilder();
        $builder
            ->setApiKey(Mage::getStoreConfig('bepado_apikey/general/apikey'))
            ->setApiEndpointUrl(Mage::getUrl('bepado/sdk/handle'))
            ->configurePDOGateway($writeConnection->getConnection())
            ->setProductToShop(new QafooLabs_Bepado_Model_MagentoProductToShop($attributes))
            ->setProductFromShop(new QafooLabs_Bepado_Model_MagentoProductFromShop($attributes))
            ->setPluginSoftwareVersion('QafooLabs Magento Plugin/' . $version)
        ;

        return $builder->build();
    }
}
