<?php

use Bepado\SDK\ProductToShop;
use Bepado\SDK\Struct;

class QafooLabs_Bepado_Model_MagentoProductToShop implements ProductToShop
{
    /**
     * Cache of Google to Magento Category Ids.
     *
     * @var array<string,string>
     */
    private $categoryCache = array();

    /**
     * Import or update given product
     *
     * Store product in your shop database as an external product. The
     * associated sourceId
     *
     * @param Struct\Product $product
     */
    public function insertOrUpdate(Struct\Product $sdkProduct)
    {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');

        $sku = 'BP-' . $sdkProduct->shopId . '-' . $sdkProduct->sourceId;
        $product = Mage::getModel('catalog/product');
        $productId = $product->getIdBySku($sku);

        if (!$productId) {
            $product = new Mage_Catalog_Model_Product();
            $product->setSku($sku);
            $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
            $product->setCreatedAt(strtotime('now'));
        } else {
            $product->load($productId);
        }

        $taxClasses = array(
            0.19 => 0,
            0.07 => 0
        ); // TODO: configure

        $defaultWebsiteId = 1; // TODO: configure

        $weight = isset($sdkProduct->attributes['weight'])
            ? $sdkProduct->attributes['weight']
            : 0.0;

        $product->setAttributeSetId(4); // TODO: configure
        $product->setTypeId('simple');
        $product->setName($sdkProduct->title);
        $product->setWeight($weight);
        $product->setCategoryIds($this->convertToMagentoCategories($sdkProduct));
        $product->setWebsiteIDs(array($defaultWebsiteId));
        $product->setDescription($sdkProduct->longDescription);
        $product->setShortDescription($sdkProduct->shortDescription);
        $product->setPrice($sdkProduct->price);
        $product->setCost($sdkProduct->purchasePrice);
        $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $product->setTaxClassId($taxClasses[$sdkProduct->vat]);
        $product->setStockData(array(
            'is_in_stock' => $sdkProduct->availability > 0,
            'qty' => $sdkProduct->availability
        ));

        $this->importImages($sdkProduct, $product);

        $product->save();

        $sql = 'INSERT INTO `bepado_magento_product_import` (bmpi_product_id, bmpi_product_sku, bmpi_data)
                     VALUES (?, ?, ?)
                     ON DUPLICATE KEY UPDATE bmpi_data = VALUES(bmpi_data)';
        $stmt = $writeConnection->prepare($sql);
        $stmt->bindValue(1, $product->getId());
        $stmt->bindValue(2, $sku);
        $stmt->bindValue(3, serialize($sdkProduct));
        $stmt->execute();
    }

    private function importImages($sdkProduct, $product)
    {
        $i = 0;
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        foreach ($sdkProduct->images as $image) {
            $i++;

            $tmp = Mage::getBaseDir('media') . DS . 'tmp'. DS . $sdkProduct->shopId . "-" . $sdkProduct->sourceId . "-" . $i . ".tmp";
            file_put_contents($tmp, file_get_contents($image));
            $type = finfo_file($finfo, $tmp);

            switch ($type) {
                case 'image/gif':
                case 'image/png':
                case 'image/jpg':
                case 'image/jpeg':
                    $path = Mage::getBaseDir('media') . DS .
                            'tmp'. DS .
                            $sdkProduct->shopId . "-" . $sdkProduct->sourceId . "-" . $i . "." . $this->getExtension($type);

                    rename($tmp, $path);

                    if ($i === 1) {
                        $product->addImageToMediaGallery($path, array('small', 'thumbnail', 'image'), false, false);
                    } else {
                        $product->addImageToMediaGallery($path, null, false, false);
                    }

                    break;

                default:
                    unlink($tmp);
                    break;
            }
        }
    }

    private function getExtension($type)
    {
        switch ($type) {
            case 'image/gif':
                return 'gif';
            case 'image/png':
                return 'png';
            case 'image/jpg':
            case 'image/jpeg':
                return 'jpg';
        }
    }

    private function convertToMagentoCategories($sdkProduct)
    {
        $categories = array();
        foreach ($sdkProduct->categories as $categorySlug) {
            if (!array_key_exists($categorySlug, $this->categoryCache)) {
                $category = Mage::getResourceModel('catalog/category_collection')
                    ->addAttributeToSelect('google_shopping_category')
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToFilter('google_shopping_category', $categorySlug)
                    ->load()
                    ->getFirstItem();

                $this->categoryCache[$categorySlug] = $category ? $category->getId() : null;
            }

            $categories[] = $this->categoryCache[$categorySlug];
        }

        return array_filter($categories);
    }

    /**
     * Delete product with given shopId and sourceId.
     *
     * Only the combination of both identifies a product uniquely. Do NOT
     * delete products just by their sourceId.
     *
     * You might receive delete requests for products, which are not available
     * in your shop. Just ignore them.
     *
     * @param string $shopId
     * @param string $sourceId
     * @return void
     */
    public function delete($shopId, $sourceId)
    {
        $sku = 'BP-' . $sdkProduct->shopId . '-' . $sdkProduct->sourceId;
        $product = Mage::getModel('catalog/product')->load($sku, 'sku');

        if (!$product) {
            return;
        }

        // Sometimes this happens in the Store Frontend during checkProducts()
        Mage::register('isSecureArea', true);
        $product->delete();
        Mage::unregister('isSecureArea');
    }

    /**
     * Start transaction
     *
     * Starts a transaction, which includes all insertOrUpdate and delete
     * operations, as well as the revision updates.
     *
     * @return void
     */
    public function startTransaction()
    {
    }

    /**
     * Commit transaction
     *
     * Commits the transactions, once all operations are queued.
     *
     * @return void
     */
    public function commit()
    {
    }
}
