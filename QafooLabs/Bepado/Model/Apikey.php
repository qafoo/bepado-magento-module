<?php

class QafooLabs_Bepado_Model_Apikey extends Mage_Core_Model_Config_Data
{
    public function save()
    {
        $apiKey = $this->getValue(); //get the value from our config

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        try {
            $sdk->verifyKey($apiKey);
        } catch (RuntimeException $e) {
            Mage::throwException("Not a valid bepado API key. Could not connect to shop.");
        }

        return parent::save();
    }
}
