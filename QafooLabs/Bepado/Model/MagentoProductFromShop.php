<?php

use Bepado\SDK\ProductFromShop;
use Bepado\SDK\Struct;
use Bepado\SDK\Struct\Product;

class QafooLabs_Bepado_Model_MagentoProductFromShop implements ProductFromShop
{
    /**
     * @var string
     */
    private $attributes;

    public function __construct(QafooLabs_Bepado_Model_Attributes $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * Get product data
     *
     * Get product data for all the product IDs specified in the given string
     * array.
     *
     * @param string[] $ids
     * @return Struct\Product[]
     */
    public function getProducts(array $ids)
    {
        $result = array();
        foreach ($ids as $id) {
            $product = Mage::getModel('catalog/product')->load($id);
            $result[] = $this->convertToSdkProduct($product);
        }

        return $result;
    }

    private function convertToSdkProduct($product)
    {
        return new Struct\Product(array(
            'sourceId' => $product->getId(),
            'ean' => $product->getEan(),
            'title' => $product->getName(),
            'shortDescription' => $product->getShortDescription(),
            'longDescription' => $product->getDescription(),
            'price' => floatval($product->getPrice()),
            'purchasePrice' => floatval($product->getCost()),
            'url' => $product->getProductUrl(),
            'vendor' => $product->getManufacturer(),
            'vat' => 0.19, // TODO: Taxclass mapping
            'availability' => $this->convertAvailability($product),
            'attributes' => $this->convertToSdkAttributes($product),
            'deliveryWorkDays' => $product->getData($this->attributes->deliveryWorkDays),
            'deliveryDate' => $product->getData($this->attributes->deliveryDate),
            'categories' => $this->convertToSdkCategories($product),
            'images' => $this->convertToSdkImages($product),
        ));
    }

    private function convertToSdkImages($product)
    {
        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_read');

        $imageBaseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product';
        $sql = 'SELECT g.value
                  FROM catalog_product_entity_media_gallery g
            INNER JOIN catalog_product_entity_media_gallery_value gv ON g.value_id = gv.value_id
                 WHERE gv.disabled = 0 and g.entity_id = ' . $db->quote($product->getId()) . '
              ORDER BY gv.position ASC';

        return array_map(
            function ($row) use ($imageBaseUrl) {
                return $imageBaseUrl . $row['value'];
            },
            $db->fetchAll($sql)
        );
    }

    private function convertToSdkCategories($product)
    {
        $categoryIds = $product->getCategoryIds();

        $googleCategories = array();

        foreach($categoryIds as $categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $googleCategories[] = $category->getData('google_shopping_category');
        }

        return array_filter(array_unique($googleCategories));
    }

    private function convertToSdkAttributes($product)
    {
        $attributes = array();

        if ($product->getWeight()) {
            $attributes[Product::ATTRIBUTE_WEIGHT] = floatval($product->getWeight());
        }

        if ($product->getData($this->attributes->unit)) {
            $attributes[Product::ATTRIBUTE_UNIT] = $product->getData($this->attributes->unit);
        }

        if ($product->getData($this->attributes->refQuantity)) {
            $attributes[Product::ATTRIBUTE_REFERENCE_QUANTITY] = $product->getData($this->attributes->refQuantity);
        }

        if ($product->getData($this->attributes->quantity)) {
            $attributes[Product::ATTRIBUTE_QUANTITY] = $product->getData($this->attributes->quantity);
        }

        return $attributes;
    }

    private function convertAvailability($product)
    {
        $data = $product->getStockData();
        $availability = 0;

        if (isset($data['qty'])) {
            $availability = $data['qty'];
        } else if (isset($data['is_in_stock'])) {
            $availability = $data['is_in_stock'] ? 1 : 0;
        }

        return $availability;
    }

    /**
     * Get all IDs of all exported products
     *
     * @return string[]
     */
    public function getExportedProductIDs()
    {
        throw new \RuntimeException("Not implemented for this plugin.");
    }

    /**
     * Reserve a product in shop for purchase
     *
     * @param Struct\Order $order
     * @return void
     * @throws \Exception Abort reservation by throwing an exception here.
     */
    public function reserve(Struct\Order $order)
    {
    }

    /**
     * Buy products mentioned in order
     *
     * Should return the internal order ID.
     *
     * @param Struct\Order $order
     * @return string
     *
     * @throws \Exception Abort buy by throwing an exception,
     *                    but only in very important cases.
     *                    Do validation in {@see reserve} instead.
     */
    public function buy(Struct\Order $order)
    {
        $quote = Mage::getModel('sales/quote')
                ->setStoreId(Mage::app()->getStore('default')->getId());

        foreach ($order->orderItems as $orderItem) {
            $product = Mage::getModel('catalog/product')->load($orderItem->product->sourceId);
            $buyInfo = array('qty' => $orderItem->count);
            $quote->addProduct($product, new Varien_Object($buyInfo));
        }

        $quote->getBillingAddress()
                ->addData($this->convertToAddressData($order->billingAddress));

        $shippingRate = Mage::getModel('shipping/rate_result_method');
        $shippingRate->setCarrier('bepado');
        $shippingRate->setCarrierTitle('bepado');

        $shippingRate->setMethod('bepado');
        $shippingRate->setMethodTitle('bepado');

        $shippingRate->setPrice($order->grossShippingCosts);
        $shippingRate->setCost(0);

        $rate = Mage::getModel('sales/quote_address_rate')
            ->importShippingRate($shippingRate);

        $quote->getShippingAddress()
                ->addData($this->convertToAddressData($order->deliveryAddress))
                ->setShippingMethod('bepado_bepado')
                ->addShippingRate($rate)
                ->setPaymentMethod('purchase_order');

        $quote->setCheckoutMethod('guest')
                    ->setCustomerId(null)
                    ->setCustomerEmail($quote->getBillingAddress()->getEmail())
                    ->setCustomerIsGuest(true)
                    ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);

        $quote->getPayment()->importData( array('method' => 'checkmo'));

        $quote->save();

        $service = Mage::getModel('sales/service_quote', $quote);
        $service->submitAll();

        $order = $service->getOrder();

        return $order->getId();
    }

    private function convertToAddressData($address)
    {
        $resource = Mage::getSingleton('core/resource');
        $db = $resource->getConnection('core_read');
        $country = $db->fetchRow('SELECT country_id FROM directory_country WHERE iso3_code = ' . $db->quote($address->country));

        $addressData = array(
            'firstname' => $address->firstName,
            'lastname' => $address->surName,
            'company' => $address->company,
            'email' =>  $address->email,
            'street' => array_filter(array($address->street . ' ' . $address->streetNumber, $address->additionalAddressLine)),
            'city' => $address->city,
            'postcode' => $address->zip,
            'country_id' => $country['country_id'],
            'telephone' => $address->phone,
            'fax' => '',
            'region_id' => '',
            'region' => '',
            'customer_password' => '',
            'confirm_password' =>  '',
            'save_in_address_book' => '0',
            'use_for_shipping' => '1',
        );

        return $addressData;
    }
}
