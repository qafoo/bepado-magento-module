<?php

use Bepado\SDK\Struct;

class QafooLabs_Bepado_Model_Observer_ShippingObserver
{
    public function onQuoteSubmitted($event)
    {
        $order = $event->getOrder();
        $items = $order->getAllItems();
        $orderItems = $this->getBepadoOrderItems($items, 'getQtyOrdered');

        if (count($orderItems) === 0) {
            return;
        }

        $sdkOrder = new Struct\Order(array(
            'deliveryAddress' => $this->convertToSdkAddress($order->getShippingAddress()),
            'orderItems' => $orderItems,
        ));

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();
        $reservation = $sdk->reserveProducts($sdkOrder);

        if (!$reservation->success) {
            /*foreach ($reservation->messages as $shopId => $messages) {
                foreach ($messages as $message) {
                }
            }*/
            throw new \RuntimeException("Aborting: " . json_encode($reservation));
        }

        $result = $sdk->checkout($reservation, $order->getIncrementId());

        $shipmentItems = array();

        foreach ($items as $item) {
            $sku = $item->getProduct()->getSku();

            if (preg_match('(BP-([a-zA-Z0-9]+)-.*+)', $sku, $match)) {
                $shipmentId = $match[1];
            } else {
                $shipmentId = 'local';
            }

            $shipmentItems[$shipmentId][$item->getId()] = $item->getQtyOrdered();
        }

        foreach ($shipmentItems as $shipmentId => $partialShipmentItems) {
            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($partialShipmentItems);
            $shipment->save();
        }
    }

    public function onSalesQuoteTotalsCollected($event)
    {
        $quote = $event->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $items = $quote->getAllItems();
        $orderItems = $this->getBepadoOrderItems($items, 'getQty');

        $hasLocalItems = (count($items) > count($orderItems));

        if (count($orderItems) === 0) {
            return;
        }

        $order = new Struct\Order(array(
            'deliveryAddress' => $this->convertToSdkAddress($shippingAddress),
            'orderItems' => $orderItems,
        ));

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        $shipping = $sdk->calculateShippingCosts($order);

        if (!$shipping->isShippable) {
            $shippingAddress->removeAllShippingRates();
            $shippingAddress
                ->setShippingAmount(0)
                ->setBaseShippingAmount(0)
                ->setShippingMethod('')
                ->setShippingDescription('')
                ->save();

            return;
        }

        $shippingAmount = $hasLocalItems ? $shippingAddress->getShippingAmount() : 0;
        $baseShippingAmount = $hasLocalItems ? $shippingAddress->getBaseShippingAmount() : 0;
        $shippingDescription = $hasLocalItems
            ? $shippingAddress->getShippingDescription() . ' + bepado.com Marktplatz-Artikel'
            : 'bepado.com Marktplatzartikel';

        $shippingAddress
            ->setShippingAmount($shippingAmount + $shipping->grossShippingCosts)
            ->setBaseShippingAmount($baseShippingAmount + $shipping->shippingCosts)
            ->setShippingDescription($shippingDescription)
            ->save();

        $rates = $shippingAddress->getAllShippingRates();

        foreach ($rates as $rate) {
            // This doesnt work because magento fetches title from config in getCarrierName($code) block method.
            /*$carrierTitle = $hasLocalItems
                ? $rate->getCarrierTitle()  . ' + bepado.com Marktplatz-Artikel'
                : 'bepado.com Marktplatzartikel';

            $rate->setCarrierTitle($carrierTitle);*/

            $ratePrice = $hasLocalItems ? $rate->getPrice() : 0;
            $rate->setPrice($ratePrice + $shipping->grossShippingCosts);
            $rate->save();
        }
    }

    private function getBepadoOrderItems($items, $qtyMethod)
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $skus = array();
        $counts = array();

        foreach ($items as $item) {
            $product = $item->getProduct();
            $skus[] = $readConnection->quote($product->getSku());
            $counts[$product->getSku()] = (int)$item->$qtyMethod();
        }

        if (count($skus) === 0) {
            return;
        }

        $sql = 'SELECT bmpi_product_sku, bmpi_data FROM bepado_magento_product_import WHERE bmpi_product_sku IN (' .  implode(", ", $skus) . ')';
        $stmt = $readConnection->prepare($sql);
        $stmt->execute();

        $orderItems = array();
        while ($row = $stmt->fetch()) {
            $orderItems[] = new Struct\OrderItem(array(
                'product' => unserialize($row['bmpi_data']),
                'count' => (int)$counts[$row['bmpi_product_sku']]
            ));
        }

        return $orderItems;
    }

    private function convertToSdkAddress($address)
    {
        $streetData = $address->getStreet();

        $street = $streetNumber = null;
        if (preg_match('(\s+(\d.*))', $streetData[0], $matches)) {
            $street = str_replace($matches[0], '', $streetData[0]);
            $streetNumber = $matches[1];
        } else if (isset($streetData[0])) {
            $street = $streetData[0];
            $streetNumber = "";
        }

        $country = Mage::getModel('directory/country')->load($address->getCountryId());

        return new Struct\Address(array(
            'firstName' => $address->getFirstname(),
            'middleName' => $address->getMiddlename(),
            'surName' => $address->getLastname(),
            'company' => $address->getCompany(),
            'street' => $street,
            'streetNumber' => $streetNumber,
            'additionalAddressLine' => isset($street[1]) ? $street[1] : null,
            'city' => $address->getCity(),
            'zip' => $address->getPostcode(),
            'state' => $address->getRegion(),
            'phone' => $address->getTelephone(),
            'email' => $address->getEmail(),
            'country' => $country->getIso3Code(),
        ));
    }
}
