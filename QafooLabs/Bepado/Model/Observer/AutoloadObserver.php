<?php

class QafooLabs_Bepado_Model_Observer_AutoloadObserver extends Varien_Event_Observer
{
    public function controllerFrontInitBefore($event)
    {
        spl_autoload_register(function ($class) {
            if (strpos($class, 'Bepado\\SDK') === 0) {
                $file = str_replace('\\', '/', $class) . '.php';
                require_once __DIR__ . '/../../vendor/bepado/sdk/src/main/' . $file;
            }
        }, true, true);
    }
}
