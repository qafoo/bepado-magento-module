<?php

class QafooLabs_Bepado_Model_Observer_QuoteAddItemObserver
{
    public function onItemAddedToQuote($event)
    {
        $item = $event->getQuoteItem();
        $quote = $item->getQuote();

        $product = $item->getProduct();

        if (!$product->getSku()) {
            return;
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $sql = 'SELECT bmpi_data FROM bepado_magento_product_import WHERE bmpi_product_sku = ?';
        $stmt = $readConnection->prepare($sql);
        $stmt->bindValue(1, $product->getSku());
        $stmt->execute();

        $bepadoData = $stmt->fetchColumn();

        if (!$bepadoData) {
            return;
        }

        $sdkProduct = unserialize($bepadoData);

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        try {
            $messages = $sdk->checkProducts(array($sdkProduct));

            if ($result !== true) {
                foreach ($result as $message) {
                    $quote->addMessage($message->message);
                }
            }
        } catch (\Exception $e) {
            echo((string)$e);die;
            $shop = $sdk->getShop($sdkProduct->shopId);
            $quote->addMessage('Cannot connect to Marketplace ' . $shop->name);

            throw $e;
        }
    }
}
