<?php

class QafooLabs_Bepado_Model_Observer_ProductBulkObserver
{
    public function addMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) !== 'Mage_Adminhtml_Block_Widget_Grid_Massaction') {
            return;
        }

        if ($block->getRequest()->getControllerName() !== 'catalog_product') {
            return;
        }

        $block->addItem('bepado_export', array(
            'label' => 'bepado Export',
            'url' => Mage::app()->getStore()->getUrl('bepado/product/export'),
        ));
    }
}
