<?php

class QafooLabs_Bepado_Model_Observer_ProductChangeObserver
{
    /**
     * @var array
     */
    private $productIds = array();

    public function onProductAttributeChanges($observer)
    {
        $this->productIds = $observer->getEvent()->getProductIds();
    }

    public function onProductMassActionEnded($observer)
    {
        $this->recordProductIdChanges($this->productIds);
    }

    public function onProductChanged($observer)
    {
        $product = $observer->getEvent()->getProduct();

        if (!$product->hasDataChanges()) {
            return;
        }

        $this->recordProductIdChanges(array($product->getId()));
    }

    private function recordProductIdChanges(array $productIds)
    {
        if (count($productIds) === 0) {
            return;
        }

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $productIds = array_map(array($readConnection, 'quote'), $productIds);

        $sql = 'SELECT bmpe_product_id FROM bepado_magento_product_export WHERE bmpe_product_id IN (' . implode(', ', $productIds) . ')';
        $stmt = $readConnection->prepare($sql);
        $stmt->execute();

        $registry = Mage::getSingleton('qafoolabs_bepado/sdkRegistry');
        $sdk = $registry->getSDK();

        foreach ($stmt->fetchAll() as $row) {
            $sdk->recordUpdate($row['bmpe_product_id']);
        }
    }
}
