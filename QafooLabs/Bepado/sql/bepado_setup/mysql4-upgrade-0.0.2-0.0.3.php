<?php

$installer = $this;
$installer->startSetup();
$installer->run(<<<SQL
CREATE TABLE IF NOT EXISTS `bepado_magento_product_import` (
    `bmpi_id` INT(11) NOT NULL PRIMARY KEY,
    `bmpi_product_sku` VARCHAR(64) NOT NULL,
    `bmpi_data` LONGBLOB NOT NULL,
    INDEX (`bmpi_product_sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SQL
);
$installer->endSetup();
