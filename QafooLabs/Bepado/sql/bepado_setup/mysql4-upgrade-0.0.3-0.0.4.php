<?php

$installer = $this;
$installer->startSetup();
$installer->run(<<<SQL
ALTER TABLE `bepado_magento_product_import`
    DROP COLUMN `bmpi_id`,
    ADD PRIMARY KEY(`bmpi_product_sku`)
;

SQL
);
$installer->endSetup();

