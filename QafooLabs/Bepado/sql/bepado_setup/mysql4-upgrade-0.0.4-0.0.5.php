<?php

$installer = $this;
$installer->startSetup();
$installer->run(<<<SQL
CREATE TABLE `bepado_magento_product_export` (
    `bmpe_product_id` INT(11) NOT NULL,
    PRIMARY KEY (`bmpe_product_id`)
) ENGINE=InnoDB;

SQL
);
$installer->endSetup();
