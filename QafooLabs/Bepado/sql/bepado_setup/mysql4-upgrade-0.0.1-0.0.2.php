<?php

$installer = $this;
$installer->startSetup();
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'google_shopping_category', array(
    'group'         => 'Bepado',
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Google Shopping Category',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
$installer->endSetup();
