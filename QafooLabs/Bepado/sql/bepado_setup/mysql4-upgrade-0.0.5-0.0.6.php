<?php

$installer = $this;
$installer->startSetup();
$installer->run(<<<SQL
ALTER TABLE `bepado_magento_product_import`
    ADD COLUMN `bmpi_product_id` INT(11) NOT NULL,
    DROP PRIMARY KEY
;

UPDATE bepado_magento_product_import i, catalog_product_entity e
SET i.bmpi_product_id = e.entity_id
WHERE i.bmpi_product_sku = e.sku;

ALTER TABLE `bepado_magento_product_import`
    ADD PRIMARY KEY(`bmpi_product_id`)
;

SQL
);
$installer->endSetup();
