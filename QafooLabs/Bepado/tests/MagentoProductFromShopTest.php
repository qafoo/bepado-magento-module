<?php

use Bepado\SDK\Struct;

class MagentoProductFromShopTest extends \PHPUnit_Framework_TestCase
{
    public function testBuyProducts()
    {
        $order = new Struct\Order();

        $order->deliveryAddress = new Struct\Address(
            array(
                'name' => 'John Doe',
                'line1' => 'Foo-Street 42',
                'zip' => '12345',
                'city' => 'Sindelfingen',
                'country' => 'DEU',
                'email' => 'foo@qafoo.com',
                'phone' => '+12345678',
            )
        );
        $order->billingAddress = $order->deliveryAddress;

        $order->orderItems[] = new Struct\OrderItem(
            array(
                'count' => 1,
                'product' => new Struct\Product(
                    array(
                        'shopId' => 'shop-1',
                        'sourceId' => '23-1234',
                        'price' => 20,
                        'purchasePrice' => 10,
                        'fixedPrice' => false,
                        'currency' => 'EUR',
                        'availability' => 5,
                        'title' => 'Sindelfingen',
                        'categories' => array('/others'),
                    )
                ),
            )
        );

        $fromShop = new QafooLabs_Bepado_Model_MagentoProductFromShop(
            new QafooLabs_Bepado_Model_Attributes()
        );
        $fromShop->buy($order);
    }
}
